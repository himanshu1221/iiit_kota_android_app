package in.forsk.iiitk;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

 //   main class is made by ankit kaushik


public class _2013KUCP1023_TIme_Table_Main_Activity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private String jsonData;
    private String url;

    private _2013KUCP1023_Time_Table_Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__2013_kucp1023__time__table__main_);

        recyclerView = (RecyclerView) findViewById(R.id.timeTable1RCV);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        url = "http://online.mnit.ac.in/iiitk/assets/timetable5.json";

        class WorkerThread extends Thread{
            @Override
            public void run() {
                try {
                    jsonData = openHttpConnection(url);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        WorkerThread workerThread = new WorkerThread();
        workerThread.start();
        try {
            workerThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        try {
            adapter = new _2013KUCP1023_Time_Table_Adapter(getApplicationContext() , getTimeTableList(jsonData));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        recyclerView.setAdapter(adapter);

    }
    
    
    //    here json code is fetched by Himanshu Goyal

    public List<_2013KUCP1023_Time_Table_Wrapper> getTimeTableList(String jsonData) throws JSONException {


        List<_2013KUCP1023_Time_Table_Wrapper> list = new ArrayList<_2013KUCP1023_Time_Table_Wrapper>();

        JSONArray rootArray = null;

        try {
            rootArray = new JSONArray(jsonData);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        for (int i=0;i<rootArray.length();i++){
            JSONObject jsonObject = rootArray.getJSONObject(i);

            _2013KUCP1023_Time_Table_Wrapper wrapper = new _2013KUCP1023_Time_Table_Wrapper();

            String date = jsonObject.getString("Date");
            String time = jsonObject.getString("Time");
            String faculty = jsonObject.getString("Faculty");
            String subject = jsonObject.getString("Subject");

            wrapper.setDate(date);
            wrapper.setBatch(subject);
            wrapper.setFaculty(faculty);
            wrapper.setTime(time);

            list.add(wrapper);

        }


        return list;
    }

    
    //  done by both of us together 
    
    private String openHttpConnection(String urlStr) throws IOException {
        InputStream in = null;
        int resCode = -1;

        try {
            URL url = new URL(urlStr);
            URLConnection urlConn = url.openConnection();

            if (!(urlConn instanceof HttpURLConnection)) {
                throw new IOException("URL is not an Http URL");
            }

            HttpURLConnection httpConn = (HttpURLConnection) urlConn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            resCode = httpConn.getResponseCode();
            if (resCode == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return convertStreamToString(in);
    }

    private String convertStreamToString(InputStream is) throws IOException {
        // Converting input stream into string
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int i = is.read();
        while (i != -1) {
            baos.write(i);
            i = is.read();
        }
        return baos.toString();
    }


}

