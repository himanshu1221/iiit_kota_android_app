package in.forsk.iiitk;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;


// done by Himanshu Goyal


public class _2013KUCP1023_Time_Table_Adapter extends RecyclerView.Adapter<_2013KUCP1023_Time_Table_Adapter.ViewHolder> {

	 private Context context;
	    private List<_2013KUCP1023_Time_Table_Wrapper> list;

	    public _2013KUCP1023_Time_Table_Adapter(Context applicationContext, List<_2013KUCP1023_Time_Table_Wrapper> timeTableList) {
	        context = applicationContext;
	        list = timeTableList;
	    }

	    @Override
	    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

	        View rootView = LayoutInflater.from(context).inflate(R.layout._2013kucp1023_time_table_list_row , parent , false);

	        ViewHolder holder = new ViewHolder(rootView);

	        return holder;
	    }

	    @Override
	    public void onBindViewHolder(ViewHolder holder, int position) {

	        holder.tv1.setText(list.get(position).getDate());
	        holder.tv2.setText(list.get(position).getTime());
	        holder.tv3.setText(list.get(position).getBatch());
	        holder.tv4.setText(list.get(position).getFaculty());

	    }

	    @Override
	    public int getItemCount() {
	        return list.size();
	    }

	    public class ViewHolder extends RecyclerView.ViewHolder {

	        private TextView tv1;
	        private TextView tv2;
	        private TextView tv3;
	        private TextView tv4;

	        public ViewHolder(View itemView) {
	            super(itemView);

	            tv1 = (TextView) itemView.findViewById(R.id.dateRow);
	            tv2 = (TextView) itemView.findViewById(R.id.timeRow);
	            tv3 = (TextView) itemView.findViewById(R.id.batchRow);
	            tv4 = (TextView) itemView.findViewById(R.id.facultyRow);

	        }
	    }
	}
