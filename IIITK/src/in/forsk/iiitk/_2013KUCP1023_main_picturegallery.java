package in.forsk.iiitk;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

// made by himanshu goyal


public class _2013KUCP1023_main_picturegallery extends AppCompatActivity implements ItemClickListener {


    private RecyclerView recyclerView;
    private GridLayoutManager layoutManager;
    private _2013KUCP1023_PictureGalleryAdapter adapter;
    static List<_2013KUCP1023_ImageWrapper> wrapperList;
    private String jsonUrl;
    private String jsonData;
    private _2013KUCP1023_ConnectionDetector cd;
    private boolean isConnection = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__2013_kucp1023_main_picturegallery);

        cd = new _2013KUCP1023_ConnectionDetector(getApplicationContext());
        isConnection = cd.isConnectingToInternet();

        wrapperList = new ArrayList<_2013KUCP1023_ImageWrapper>();
        jsonUrl = "http://online.mnit.ac.in/iiitk/assets/picturegallery.txt";

        if (!isConnection){
            Toast.makeText(getApplicationContext() , "No Internet Connection" , Toast.LENGTH_LONG).show();
        }

        if (isConnection) {
//            FetchJSONFOnline fetch = new FetchJSONFOnline(this , this);
//            fetch.execute(jsonUrl);


            class WorkerThread extends Thread {

                @Override
                public void run() {
                    try {
                        jsonData = openHttpConnection(jsonUrl);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            WorkerThread workerThread = new WorkerThread();

            workerThread.start();

            try {
                workerThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


            try {
                wrapperList = getImagesByJSON(jsonData);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

        recyclerView = (RecyclerView) findViewById(R.id.pgRcv);
        layoutManager = new GridLayoutManager(getApplicationContext() , 2);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new _2013KUCP1023_PictureGalleryAdapter(getApplicationContext() , _2013KUCP1023_main_picturegallery.wrapperList);
        recyclerView.setAdapter(adapter);

        adapter.setItemClickListener(this);
    }

    private String openHttpConnection(String urlStr) throws IOException {
        InputStream in = null;
        int resCode = -1;

        try {
            URL url = new URL(urlStr);
            URLConnection urlConn = url.openConnection();

            if (!(urlConn instanceof HttpURLConnection)) {
                throw new IOException("URL is not an Http URL");
            }

            HttpURLConnection httpConn = (HttpURLConnection) urlConn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            resCode = httpConn.getResponseCode();
            if (resCode == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return convertStreamToString(in);
    }

    private String convertStreamToString(InputStream is) throws IOException {
        // Converting input stream into string
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        if (is != null) {
            int i = is.read();
            while (i != -1) {
                baos.write(i);
                i = is.read();
            }
        }
        return baos.toString();
    }

    public List<_2013KUCP1023_ImageWrapper> getImagesByJSON(String jsonString) throws JSONException, IOException {

        List<_2013KUCP1023_ImageWrapper> wrapperList = new ArrayList<_2013KUCP1023_ImageWrapper>();

        JSONObject jsonRootObject = new JSONObject(jsonString);

        for (int i=0;i<jsonRootObject.length();i++){

            _2013KUCP1023_ImageWrapper imageWrapper = new _2013KUCP1023_ImageWrapper();

            List<String> list = new ArrayList<String>();

            JSONObject groupObject = jsonRootObject.getJSONObject("group" + (i + 1));
            String groupName = groupObject.getString("Name");
            String groupImageLink = groupObject.getString("GroupImageLink");
            JSONArray jsonArray = groupObject.getJSONArray("ImageLink");

            for (int j=0;j<jsonArray.length();j++){
                String link = jsonArray.getString(j);
                list.add(link);
            }


            imageWrapper.setGroupName(groupName);
            imageWrapper.setImageLink(list);
            imageWrapper.setGroupImageLink(groupImageLink);

            wrapperList.add(imageWrapper);
        }

        return wrapperList;
    }



    @Override
    public void onClickListener(View v, int position) {
        Intent  intent = new Intent(getApplicationContext() , _2013KUCP1023_gallery.class);
        intent.putExtra("groupPosition",position);
        startActivity(intent);
        finish();
    }

}