package in.forsk.iiitk;


//  made by himasnhu goyal

import android.app.Activity;


import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class _2013KUCP1023_gallery extends AppCompatActivity implements ItemClickListener1 {

	  private RecyclerView recyclerView;
	    private GridLayoutManager layoutManager;
	    private _2013KUCP1023_GalleryAdapter adapter;
	    private RelativeLayout toolbarHolder;
	    static TextView toolBarHeader;
	    private ImageView backButton;

	    @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity__2013_kucp1023_gallery);


	        Bundle bundle = getIntent().getExtras();
	        int groupPosition = bundle.getInt("groupPosition");

	        recyclerView = (RecyclerView) findViewById(R.id.gRcv);
	        layoutManager = new GridLayoutManager(getApplicationContext() , 3);
	        recyclerView.setLayoutManager(layoutManager);

	        adapter = new _2013KUCP1023_GalleryAdapter(getApplicationContext() , groupPosition);
	        recyclerView.setAdapter(adapter);

	        adapter.setItemClickListener(this);

	        toolbarHolder = (RelativeLayout) findViewById(R.id.galleryToolbar);
	        toolBarHeader = (TextView) toolbarHolder.findViewById(R.id.toolBarText);
	        backButton = (ImageView) toolbarHolder.findViewById(R.id.backButtonToolbar);

	        backButton.setOnClickListener(new View.OnClickListener() {
	            @Override
	            public void onClick(View view) {
	                startActivity(new Intent(getApplicationContext() , _2013KUCP1023_main_picturegallery.class));
	                finish();
	            }
	        });

	    }

	    @Override
	    public boolean onCreateOptionsMenu(Menu menu) {
	        // Inflate the menu; this adds items to the action bar if it is present.
	        getMenuInflater().inflate(R.menu._2013_kucp1023_gallery, menu);
	        return true;
	    }

	    @Override
	    public boolean onOptionsItemSelected(MenuItem item) {
	        // Handle action bar item clicks here. The action bar will
	        // automatically handle clicks on the Home/Up button, so long
	        // as you specify a parent activity in AndroidManifest.xml.
	        int id = item.getItemId();

	        //noinspection SimplifiableIfStatement
	        if (id == R.id.action_settings) {
	            return true;
	        }

	        return super.onOptionsItemSelected(item);
	    }

	    @Override
	    public void onClickListener(View v, int position , int groupPosition) {
	        Intent intent = new Intent(getApplicationContext() , _2013kucp1023_expandedwallpaper.class);
	        intent.putExtra("position",position);   //put link of image at position+
	        intent.putExtra("groupPosition" , groupPosition);

	        startActivity(intent);
	    }
	}
