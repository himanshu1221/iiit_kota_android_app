package in.forsk.iiitk;

//  made by together

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.squareup.picasso.Picasso;

public class _2013kucp1023_expandedwallpaper extends AppCompatActivity {

	private ImageView expandedWallpaper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__2013kucp1023_expandedwallpaper);

        Bundle bundle = getIntent().getExtras();

        int p = bundle.getInt("position");
        int gp = bundle.getInt("groupPosition");

        expandedWallpaper  = (ImageView) findViewById(R.id.ewImage);

        Picasso.with(getApplicationContext()).load(_2013KUCP1023_main_picturegallery.wrapperList.get(gp).getImageLink().get(p)).into(expandedWallpaper);
        //expandedWallpaper.setImageResource(R.drawable.wallpaper);  //Access link from json


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu._2013kucp1023_expandedwallpaper, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
